package level1

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Random, Success}

object Main extends App {

  var r : Random = new Random()

  // This is the example of web scala : https://docs.scala-lang.org/overviews/core/futures.html#blocking
  val coinAQuote = Future { Thread.sleep(r.nextInt(5000)); r.nextInt(500) }
  val coinBQuote = Future { Thread.sleep(r.nextInt(5000)); r.nextInt(1000) }
  def isProfitable(i: Int, i1: Int) = {
    println("Value A = " + i + " Value B = " + i1)
    i<i1
  }
  def buy(i: Int, i1: Int) = {
    println(s"Buy $i1, total amount " + i*i1)
    i*i1
  }

  val purchase = coinAQuote flatMap {  /* flatMap :: Future[Int] -> (Int -> Future[Int]) -> Future[Int] */
                 /*Future*/
    a =>
  /*Int*/
      coinBQuote
      /*Future*/
        .filter(b => isProfitable(a, b)) /*withFilter :: Future[Int] -> (Int -> Boolean) -> Future[Int]*/
                 /*Int*/   /*Int*/
        .map(b => buy(1, b)) /* map :: Future[Int] -> (Int -> Int) -> Future[Int] */
          /*Int*/ /*Int*/
  }
  purchase onComplete {
    case Success(result) => println(result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  println(purchase.isCompleted)

  while(!purchase.isCompleted) {}

}
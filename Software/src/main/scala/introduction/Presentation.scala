package introduction

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object Presentation extends App {

  val fut = Future {Thread.sleep(2000); 1+1}

  fut onComplete {
    case Success(result) => println("fut value " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }
  println("future is complete " + fut.isCompleted)

  // Creating futures
  /**
    * Wee have three factories
    */
  // successful
  val futNew1 = Future.successful{1+1}
  futNew1 onComplete {
    case Success(result) => println("futNew1 value " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }
  // failed
  val futNew2 = Future failed{new Exception("Boom")}
  futNew2 onComplete {
    case Success(result) => println("futNew2 value " + result)
    case Failure(t) => println("futNew2. An error has occured: " + t.getMessage)
  }
  // fromTry
  val futNew3 = Future fromTry (Success{1+2})
  futNew3 onComplete {
    case Success(result) => println("futNew3 value " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  while(!fut.isCompleted
    || !futNew1.isCompleted
    || !futNew2.isCompleted
    || !futNew3.isCompleted){}
}

package introduction

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}


object Transformation extends App {

  val fut: Future[Int] = Future {Thread.sleep(2000); 1+2}

  // Map
  val futSum: Future[Int] = fut map { x => x+1 }
  futSum onComplete {
    case Success(result) => println("futSum value " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }
  println("future futSum is complete " + futSum.isCompleted)

  // For
  val futFor = for {
    x <- fut
    y <- futSum
  } yield x+y
  futFor onComplete {
    case Success(result) => println("futFor value " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  // FlatMap, filter
  // Execution Ok
  val futFlatMapOk = fut flatMap {
    x => futSum.filter(x => x>1).map(z => z+10)
// Int -> Future[Int]
  }
  futFlatMapOk onComplete {
    case Success(result) => println("futFlatMapOk value " + result)
    case Failure(t) => println("futFlatMap. An error has occured: " + t.getMessage)
  }
  // Error
  val futFlatMapError = fut flatMap {
    x => futSum.filter(y => y>10).map(z => z+10)
  }
  futFlatMapError onComplete {
    case Success(result) => println("futFlatMapError `value " + result)
    case Failure(t) => println("futFlatMap. An error has occured: " + t.getMessage)
  }

  // Zip. The zip method will transform two successful futures into a future tuple of both values

  val zipF : Future[(Int,Int)] = fut zip futFlatMapOk
  zipF onComplete {
    case Success(result) => {
      println("zipF value " + result + "\n" + "  fut " + fut.value + "\n"+ "  futFlatMapOk " + futFlatMapOk.value)
    }
    case Failure(t) => println("zipF. An error has occured: " + t.getMessage)
  }

  while(!futSum.isCompleted
    || !fut.isCompleted
    || !futFor.isCompleted
    || !futFlatMapOk.isCompleted
    || !futFlatMapError.isCompleted
    || !zipF.isCompleted){}
}

package introduction

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

object Block extends App {

  val futSlow = Future {Thread.sleep(5000); 5+11}
  futSlow onComplete {
    case Success(result) => println("futSlow value " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  println("future is complete " + futSlow.isCompleted)

  Await.result(futSlow, Duration(100, "seconds"))

}

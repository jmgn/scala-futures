package introduction

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}

object Promises extends App {

  val p = Promise[Int]()
  val f = p.future

  val producer = Future {
    val partialResult = 3
    p success partialResult
  }
  val consumer = Future {
    val result = 7
    f onComplete {
      case Success(r) => println("comsumer value " + r.+(result))
      case Failure(t) => println("An error has occured: " + t.getMessage)
    }
  }

  println("producer is complete " + producer.isCompleted)
  println("consumer is complete " + consumer.isCompleted)

  while(!consumer.isCompleted){}

  println("producer is complete " + producer.isCompleted)
  println("consumer is complete " + consumer.isCompleted)

}

package introduction

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object Concurrent extends App {

  /**
    * In the expression for the futures do not run parallel
    */
  val initfutForNoParallel = System.currentTimeMillis()

  val futForNoParallel = for {
    x <- Future {Thread.sleep(1000); 1+1}
    y <- Future {Thread.sleep(1000); 1+1}
  } yield x+y
  futForNoParallel onComplete {
    case Success(result) => println("futForNoParallel Time " + (System.currentTimeMillis() - initfutForNoParallel) + " Result " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  println("future is complete " + futForNoParallel.isCompleted)

  /**
    * Because the futures are declared out of for, the languaje can run then operation in parellel.
    * See the console, the time of execute this future is not the sum of the two futures
    */
  val initfutForParallel = System.currentTimeMillis()
  val futSumSleep = Future {Thread.sleep(1000); 1+1}
  val futSumSleep1 = Future {Thread.sleep(1000); 1+1}
  val futForParallel = for {
    x <- futSumSleep
    y <- futSumSleep1
  } yield x+y
  futForParallel onComplete {
    case Success(result) => println("futForParallel Time " + (System.currentTimeMillis() - initfutForParallel) + " Result " + futForParallel)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  while(!futForNoParallel.isCompleted
    || !futForParallel.isCompleted){}

}
